import {MyAppComponent} from './app.component';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {IonicModule, Platform} from 'ionic-angular';
import {HttpClientModule} from '@angular/common/http';
import {PlatformMock} from '../../test-config/mocks-ionic';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthenticationProvider} from '../providers/authentication-provider';
import {HttpProvider} from '../providers/http.provider';
import {UserDetailsProvider} from '../providers/user-details.provider';


describe('MyApp', () => {
    let component: MyAppComponent;
    let fixture: ComponentFixture<MyAppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyAppComponent),
                HttpClientModule
            ],
            declarations: [
                MyAppComponent
            ],
            providers: [
                {provide: Platform, useClass: PlatformMock},
                StatusBar,
                SplashScreen,
                AuthenticationProvider,
                HttpProvider,
                UserDetailsProvider
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MyAppComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
