import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {MyAppComponent} from './app.component';
import {LoginPageModule} from '../pages/login/login.module';
import {HttpProvider} from '../providers/http.provider';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {UserDetailsProvider} from '../providers/user-details.provider';
import {ComponentsModule} from '../components/components.module';
import {AuthenticationProvider} from '../providers/authentication-provider';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ConfigProvider} from '../providers/config.provider';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyAppComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyAppComponent),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        LoginPageModule,
        HttpClientModule,
        ComponentsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyAppComponent
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        HttpProvider,
        UserDetailsProvider,
        AuthenticationProvider,
        ConfigProvider
    ]
})
export class AppModule {
}
