export class AccountDetails {

  public accountId: string;
  public costPerMonth: number;
  public payingEnough: boolean;
  public endOfPlanBalance: number;
  public lastElectricityMeterReading: number;
  public lastGasMeterReading: number;
}
