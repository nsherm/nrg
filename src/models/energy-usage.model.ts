export class EnergyUsage {
  electricyCosts: number[] = [];
  gasCosts: number[] = [];
  similarElectricyCosts: number[] = [];
  similarGasCosts: number[] = [];
}
