export class UserDetails {
  public firstName: string;
  public houseNumberOrName: string;
  public street: string;
  public town: string;
}
