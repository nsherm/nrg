export class Config {
    loginApiUrl: string;
    logoutApiUrl: string;
    userDetailsApiUrl: string;
    accountDetailsApiUrl: string;
    energyUsageApiUrl: string;
}
