import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {HomePageComponent} from './home';
import {ComponentsModule} from '../../components/components.module';
import {EnergyUsageChartComponent} from './components/energy-usage-chart/energy-usage-chart';
import {AccountOverviewComponent} from './components/account-overview/account-overview';
import {EnergyUsageProvider} from './providers/energy-usage.provider';
import {AccountDetailsProvider} from './providers/account-details.provider';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    HomePageComponent,
    EnergyUsageChartComponent,
    AccountOverviewComponent
  ],
  imports: [
    IonicPageModule.forChild(HomePageComponent),
    TranslateModule.forChild(),
    ComponentsModule,
  ],
  exports: [
    HomePageComponent
  ],
  providers: [
    EnergyUsageProvider,
    AccountDetailsProvider
  ]
})
export class HomePageModule {}
