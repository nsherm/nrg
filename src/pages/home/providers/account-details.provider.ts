import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpProvider} from '../../../providers/http.provider';
import {AccountDetails} from '../../../models/account-details.model';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertController, App} from 'ionic-angular';
import {ProviderBase} from '../../../providers/provider-base';
import {ConfigProvider} from '../../../providers/config.provider';

@Injectable()
export class AccountDetailsProvider extends ProviderBase {

  private _accountDetails: BehaviorSubject<AccountDetails> = new BehaviorSubject(null);

  constructor(private httpProvider: HttpProvider,
              private app: App,
              protected alertCtrl: AlertController,
              private configProvider: ConfigProvider) {
    super(alertCtrl);
  }

  getAccountDetails(): BehaviorSubject<AccountDetails> {
    return this._accountDetails;
  }

  lookupAccountDetails() {
    this.configProvider.ready().then((config) => {
      this.httpProvider.get<AccountDetails>(config.accountDetailsApiUrl).subscribe((response) => {
        if (response.ok) {
          this._accountDetails.next(response.body);
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.app.getActiveNav().setRoot('page-login');
        } else {
          this.handleError('Account Details Error',
              'There was an error fetching the account details data, please try again.');
        }
      });
    });
  }
}
