import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {EnergyUsage} from '../../../models/energy-usage.model';
import {HttpErrorResponse} from '@angular/common/http';
import {HttpProvider} from '../../../providers/http.provider';
import {AlertController, App} from 'ionic-angular';
import {ProviderBase} from '../../../providers/provider-base';
import {ConfigProvider} from '../../../providers/config.provider';

@Injectable()
export class EnergyUsageProvider extends ProviderBase {

  private _energyUsage: BehaviorSubject<EnergyUsage> = new BehaviorSubject(null);

  constructor(private httpProvider: HttpProvider,
              private app: App,
              protected alertCtrl: AlertController,
              private configProvider: ConfigProvider) {
    super(alertCtrl);
  }

  getEnergyUsages(): BehaviorSubject<EnergyUsage> {
    return this._energyUsage;
  }

  lookupEnergyUsage() {
    this.configProvider.ready().then((config) => {
      this.httpProvider.get<EnergyUsage>(config.energyUsageApiUrl).subscribe((response) => {
        if (response.ok) {
          this._energyUsage.next(response.body);
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.app.getActiveNav().setRoot('page-login');
        } else {
          this.handleError('Energy Usage Error',
              'There was an error fetching the energy usage data, please try again.');
        }
      });
    });
  }
}
