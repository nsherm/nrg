import {async, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AlertController, App, Config, Platform} from 'ionic-angular';
import {HttpProvider} from '../../../providers/http.provider';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {PlatformMock} from '../../../../test-config/mocks-ionic';
import {Observable} from 'rxjs';
import {EnergyUsageProvider} from './energy-usage.provider';
import {EnergyUsage} from '../../../models/energy-usage.model';

export class MockApp extends App {

}

describe('Energy Usage Provider', () => {

    let provider: EnergyUsageProvider;
    let httpProvider: HttpProvider;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                AlertController,
                HttpProvider,
                EnergyUsageProvider,
                Config,
                {provide: Platform, useClass: PlatformMock},
                {provide: App, useClass: MockApp}
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));


    beforeEach(() => {
        provider = TestBed.get(EnergyUsageProvider);
        httpProvider = TestBed.get(HttpProvider);
    });

    it('should create', () => {
        expect(provider).toBeTruthy();
    });

    it('should lookup energy usage', () => {
        // Given
        const mockEnergyUsage = new EnergyUsage();
        mockEnergyUsage.electricyCosts = [100, 100, 100];

        spyOn(httpProvider, 'get').and.returnValue(Observable.of({ok: true, body: mockEnergyUsage}));

        // When
        provider.lookupEnergyUsage();

        // Then
        expect(provider.getEnergyUsages().getValue().electricyCosts).toEqual([100, 100, 100]);
    });
});
