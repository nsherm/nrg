import {AccountDetailsProvider} from './account-details.provider';
import {async, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AlertController, App, Config, Platform} from 'ionic-angular';
import {HttpProvider} from '../../../providers/http.provider';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {PlatformMock} from '../../../../test-config/mocks-ionic';
import {AccountDetails} from '../../../models/account-details.model';
import {Observable} from 'rxjs';

export class MockApp extends App {

}

describe('Account Details Provider', () => {

    let provider: AccountDetailsProvider;
    let httpProvider: HttpProvider;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                AlertController,
                HttpProvider,
                AccountDetailsProvider,
                Config,
                {provide: Platform, useClass: PlatformMock},
                {provide: App, useClass: MockApp}
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));


    beforeEach(() => {
        provider = TestBed.get(AccountDetailsProvider);
        httpProvider = TestBed.get(HttpProvider);
    });

    it('should create', () => {
        expect(provider).toBeTruthy();
    });

    it('should lookup account details', () => {
        // Given
        const mockAccountDetails = new AccountDetails();
        mockAccountDetails.accountId = 'ABC123';

        spyOn(httpProvider, 'get').and.returnValue(Observable.of({ok: true, body: mockAccountDetails}));

        // When
        provider.lookupAccountDetails();

        // Then
        expect(provider.getAccountDetails().getValue().accountId).toEqual('ABC123');
    });
});
