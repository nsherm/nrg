import {Component} from '@angular/core';
import {IonicPage, MenuController, Platform} from 'ionic-angular';
import {AuthenticationProvider} from '../../providers/authentication-provider';


@IonicPage({
  name: 'page-home',
  segment: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePageComponent {

  constructor(private menu: MenuController,
              private platform: Platform,
              private authenticationProvider: AuthenticationProvider) {
    this.menu.swipeEnable(true);
    this.menu.enable(true);
  }

  ionViewCanEnter() {
    return this.authenticationProvider.isAuthenticated();
  }

  isMobile(): boolean {
    return this.platform.is('mobile');
  }

}

