import {Component, OnInit} from '@angular/core';
import {AccountDetails} from '../../../../models/account-details.model';
import {AccountDetailsProvider} from '../../providers/account-details.provider';

@Component({
  selector: 'account-overview',
  templateUrl: 'account-overview.html'
})
export class AccountOverviewComponent implements OnInit {

  accountDetails: AccountDetails;

  constructor(private accountDetailsProvider: AccountDetailsProvider) {
  }

  ngOnInit() {
    this.accountDetailsProvider.getAccountDetails().subscribe((accountDetails) => {
      if (accountDetails) {
        this.accountDetails = accountDetails;
      }
    });
    this.accountDetailsProvider.lookupAccountDetails();
  }
}
