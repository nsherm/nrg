import {Component, OnInit, ViewChild} from '@angular/core';
import { Chart } from 'chart.js';
import {EnergyUsageProvider} from '../../providers/energy-usage.provider';
import {EnergyUsage} from '../../../../models/energy-usage.model';

@Component({
  selector: 'energy-usage-chart',
  templateUrl: 'energy-usage-chart.html'
})
export class EnergyUsageChartComponent implements OnInit {

  showSimilarHomes = false;
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  energyUsage: EnergyUsage;

  constructor(private energyUsageProvider: EnergyUsageProvider) {

  }

  ngOnInit() {
    this.energyUsageProvider.getEnergyUsages().subscribe((energyUsage) => {
      if (energyUsage) {
        this.energyUsage = energyUsage;
        this.barChart = new Chart(this.barCanvas.nativeElement, {

          type: 'bar',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
            datasets: [{
              label: 'Electricity',
              stack: 'stack 0',
              data: energyUsage.electricyCosts,
              backgroundColor: 'rgb(40, 147, 192)'
            },
              {
                label: 'Gas',
                stack: 'stack 0',
                data: energyUsage.gasCosts,
                backgroundColor: 'rgb(47,86,192)'
              }]
          },
          options: {
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Month',
                },
              }],
              yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: '£',
                },
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }

        });
      }
    });
    this.energyUsageProvider.lookupEnergyUsage();

  }

  toggleSimilarHomes() {
    this.showSimilarHomes = !this.showSimilarHomes;
    if (this.showSimilarHomes) {
      const similarHomesElectricity = {
        label: 'Similar Electricity',
        stack: 'stack 1',
        data: this.energyUsage.similarElectricyCosts,
        backgroundColor: 'rgb(0,192,42)'
      };
      const similarHomesGas = {
        label: 'Similar Gas',
        stack: 'stack 1',
        data: this.energyUsage.similarGasCosts,
        backgroundColor: 'rgb(192,10,0)'
      };

      this.barChart.config.data.datasets[2] = similarHomesElectricity;
      this.barChart.config.data.datasets[3] = similarHomesGas;
    } else {
      this.barChart.config.data.datasets = this.barChart.config.data.datasets.splice(0, 2);
    }

    this.barChart.update();
  }
}
