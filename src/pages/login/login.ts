import { Component } from '@angular/core';
import {IonicPage, MenuController} from 'ionic-angular';
import {AuthenticationProvider} from '../../providers/authentication-provider';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@IonicPage({
  name: 'page-login',
  segment: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPageComponent {

  form: FormGroup;

  constructor(private menu: MenuController,
              private authenticationProvider: AuthenticationProvider) {

    this.menu.swipeEnable(false);
    this.menu.enable(false);

    this.buildForm();
  }

  private buildForm() {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
    });
  }

  onLogin() {
    this.authenticationProvider.login(this.form.controls['username'].value,
        this.form.controls['password'].value);
  }

}
