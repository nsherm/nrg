import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPageComponent } from './login';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    LoginPageComponent,
  ],
  imports: [
    IonicPageModule.forChild(LoginPageComponent),
    TranslateModule.forChild(),
  ],
  exports: [
    LoginPageComponent
  ]
})
export class LoginPageModule {}
