import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import { PageHeaderComponent } from './page-header/page-header';
import {PageFooterComponent} from './page-footer/page-footer';

@NgModule({
  declarations: [PageHeaderComponent,
  PageFooterComponent],
  imports: [
    IonicPageModule,
  ],
  exports: [PageHeaderComponent, PageFooterComponent]
})
export class ComponentsModule {
}
