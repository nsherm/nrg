import {Component, OnInit} from '@angular/core';
import {UserDetailsProvider} from '../../providers/user-details.provider';
import {UserDetails} from '../../models/user-details.model';

@Component({
  selector: 'page-header',
  templateUrl: 'page-header.html'
})
export class PageHeaderComponent implements OnInit {

  public userDetails: UserDetails;
  constructor(private userDetailsProvider: UserDetailsProvider) {
  }

  ngOnInit() {
    this.userDetailsProvider.getUserDetails().subscribe((userDetails) => {
      if (userDetails) {
        this.userDetails = userDetails;
      } else {
        this.userDetailsProvider.lookupUserDetails();
      }
    });


  }
}
