import {Injectable} from '@angular/core';
import {AlertController, App} from 'ionic-angular';
import {HttpProvider} from './http.provider';
import {UserDetails} from '../models/user-details.model';
import {ProviderBase} from './provider-base';
import {UserDetailsProvider} from './user-details.provider';
import {ConfigProvider} from './config.provider';

@Injectable()
export class AuthenticationProvider extends ProviderBase {

    constructor(private app: App,
                private httpProvider: HttpProvider,
                protected alertCtrl: AlertController,
                private userDetailsProvider: UserDetailsProvider,
                private configProvider: ConfigProvider) {
        super(alertCtrl);
    }

    public login(username: string, password: string) {
        this.configProvider.ready().then((config) => {
            this.httpProvider.post<UserDetails>(config.loginApiUrl, {
                username: username,
                password: password
            }).subscribe((response) => {
                if (response.ok) {
                    this.userDetailsProvider.lookupUserDetails();

                    this.app.getActiveNav().setRoot('page-home');
                }
            }, (httpError) => {
                this.handleError('Failed Login',
                    httpError.error.errorMessage);
            });
        });
    }

    public logout() {
        this.configProvider.ready().then((config) => {
            this.httpProvider.get(config.logoutApiUrl).subscribe((response) => {
                if (response.ok) {
                    this.app.getActiveNav().setRoot('page-login');
                    window.localStorage.removeItem('token');
                }
            }, () => {
                this.handleError('On No!',
                    'Something has gone wrong with the logout request, please try again');
            });
        });
    }

    public isAuthenticated(): boolean {
        return window.localStorage.getItem('token') != null;
    }
}
