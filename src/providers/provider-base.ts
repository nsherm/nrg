import {AlertController} from 'ionic-angular';

export class ProviderBase {

    constructor(protected alertCtrl: AlertController) {}

    public handleError(errorTitle: string, errorMessage: string) {
        const alert = this.alertCtrl.create({
            title: errorTitle,
            subTitle: errorMessage,
            buttons: ['OK']
        });
        alert.present();
    }
}
