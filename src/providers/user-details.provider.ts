import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {UserDetails} from '../models/user-details.model';
import {HttpErrorResponse} from '@angular/common/http';
import {HttpProvider} from './http.provider';
import {App} from 'ionic-angular';
import {ConfigProvider} from './config.provider';

@Injectable()
export class UserDetailsProvider {
  private _userDetails: BehaviorSubject<UserDetails> = new BehaviorSubject(null);

  constructor(private httpProvider: HttpProvider,
              private app: App,
              private configProvider: ConfigProvider) {
  }

  getUserDetails(): BehaviorSubject<UserDetails> {
    return this._userDetails;
  }

  lookupUserDetails() {
    this.configProvider.ready().then((config) => {
      this.httpProvider.get<UserDetails>(config.userDetailsApiUrl).subscribe((response) => {
        if (response.ok) {
          this._userDetails.next(response.body);
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.app.getActiveNav().setRoot('page-login');
        } else {
          alert('Error fetching account details');
        }
      });
    });
  }
}
