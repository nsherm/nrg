import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, Subscriber} from 'rxjs';
import {AlertController} from 'ionic-angular';

export interface IRequestOptions {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe: 'response';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable()
export class HttpProvider {

  protected static HTTP_TIMEOUT = 10000;
  private jwtToken: string;

  public constructor(protected http: HttpClient,
                     public alertCtrl: AlertController) {
    const currentJwt = window.localStorage.getItem('token');
    this.jwtToken = currentJwt ? currentJwt : '';
  }

  private static processError<T>(error: HttpErrorResponse, observer: Subscriber<HttpResponse<T>>) {
    observer.error(error);
    observer.complete();
  }

  public post<T>(endPoint: string, body: object):
    Observable<HttpResponse<T>> {

    return new Observable<HttpResponse<T>>((observer) => {
      this.http.post<T>(endPoint, body, this.createOptions())
        .timeout(HttpProvider.HTTP_TIMEOUT)
        .toPromise()
        .then((httpResponse: HttpResponse<T>) => this.processResponse<T>(httpResponse, observer))
        .catch((error: HttpErrorResponse) => HttpProvider.processError<T>(error, observer));
    });
  }

  public get<T>(endPoint: string):
    Observable<HttpResponse<T>> {

    return new Observable<HttpResponse<T>>((observer) => {
      this.http.get<T>(endPoint, this.createOptions())
        .timeout(HttpProvider.HTTP_TIMEOUT)
        .toPromise()
        .then((httpResponse: HttpResponse<T>) => this.processResponse<T>(httpResponse, observer))
        .catch((error: HttpErrorResponse) => HttpProvider.processError<T>(error, observer));
    });
  }

  private processResponse<T>(httpResponse: HttpResponse<T>, observer: Subscriber<HttpResponse<T>>) {
    this.jwtToken = httpResponse.headers.get('Authorization');
    window.localStorage.setItem('token', this.jwtToken);
    observer.next(httpResponse);
    observer.complete();
  }

  private createOptions(): IRequestOptions {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.jwtToken ? this.jwtToken : ''
    });

    return {headers: headers, observe: 'response'};
  }
}
