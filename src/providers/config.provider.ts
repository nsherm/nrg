import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Config} from '../models/config.model';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class ConfigProvider {

    private config: BehaviorSubject<Config> =
        new BehaviorSubject<Config>(new Config());

    constructor(private http: HttpClient) {
        this.loadConfiguration();
    }

    public ready(): Promise<Config> {
        return new Promise((resolve) => {
            this.config.subscribe((data: Config) => {
                if (data) {
                    resolve(data);
                }
            });
        });
    }

    protected loadConfiguration() {
        this.http.get<Config>('/assets/config.json')
            .subscribe((data) => {
                this.config.next(data);
            });
    }
}
